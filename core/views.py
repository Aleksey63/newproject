from django.shortcuts import render
from django.views import View


class Homepage(View):
    def get(self, request):
        context = {'text': 'Деньги на ваше будущее', 'title': 'Микрозаймы будущего'}
        return render(request=request, template_name='core/mainpage.html', context=context)


class About(View):
    def get(self, request):
        context = {'title': 'Информация о проекте'}
        return render(request=request, template_name='core/about.html', context=context)
