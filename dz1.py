# Задача 1
def data(day, mnth, year):
    month = {1: 'января', 2: 'февраля', 3: 'марта', 4: 'апреля', 5: 'мая', 6: 'июня', 7: 'июля', 8: 'августа',
             9: 'сентября', 10: 'октября', 11: 'ноября', 12: 'декабря'}
    return f'{day} {month.get(mnth)} {year}'


# Задача 2
def ourdict(tpl):
    dictin = {}
    for i in tpl[0:]:
        if i in dictin:
            dictin[i] += 1
        else:
            dictin[i] = 1
    return dictin


# Задача 3
def fio(dictin):
    first_name = dictin.get('first_name')
    last_name = dictin.get('last_name')
    middle_name = dictin.get('middle_name')
    if first_name is None and last_name is None:
        return "Нет данных"
    elif last_name is None and middle_name is None:
        return first_name
    elif first_name is None and middle_name is None:
        return last_name
    elif last_name is None:
        fo = first_name, middle_name
        return ' '.join(fo)
    elif middle_name is None:
        fi = last_name, first_name
        return ' '.join(fi)
    elif first_name is None:
        return last_name
    else:
        fullfio = last_name, first_name, middle_name
        return ' '.join(fullfio)


# Задача 4
import math


def is_prime(number):
    if number <= 1:
        return False
    number_sqrt = int(math.sqrt(number))
    divisors = range(2, (number_sqrt + 1))
    for element in divisors:
        if number % element == 0:
            return False
    return True


# Задача 5
def function(*args):
    args_list = [i for i in args if isinstance(i, int)]
    unique_args = set(args_list)
    sorted_args = sorted(unique_args)
    return sorted_args
