from core.views import Homepage, About

from django.urls import path

urlpatterns = [
    path('', Homepage.as_view(), name='homepage'),
    path('about/', About.as_view(), name='about'),
]
