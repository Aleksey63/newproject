class Counter:

    def __init__(self, value):
        self.value = value

    def inc(self):
        self.value += 1
        return self.value

    def dec(self):
        self.value -= 1
        return self.value


class ReverseCounter(Counter):

    def inc(self):
        self.value -= 1
        return self.value

    def dec(self):
        self.value += 1
        return self.value


def get_counter(x):
    number = x
    if number >= 0:
        return Counter(number)
    elif number < 0:
        return ReverseCounter(number)


