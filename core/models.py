from django.db import models
from django.contrib.auth import get_user_model


User = get_user_model()


class Student(models.Model):
    first_name = models.CharField('Имя', max_length=50, blank=True)
    last_name = models.CharField('Фамилия', max_length=50, blank=True)
    middle_name = models.CharField('Отчество', max_length=50, blank=True)
    contact = models.CharField('Контакт', max_length=255, blank=True)
    is_active = models.BooleanField('Активен', default=True)
    dc = models.DateTimeField('Создан', auto_now_add=True)


class Project(models.Model):
    student = models.ForeignKey(Student, verbose_name='студент', on_delete=models.PROTECT, related_name='projects')
    url = models.URLField('URL')
    dc = models.DateTimeField('создан', auto_now_add=True)


class Task(models.Model):
    students = models.ManyToManyField(Student, verbose_name='студенты', related_name='tasks')
    projects = models.ManyToManyField(Project, verbose_name='проекты', related_name='tasks')
    title = models.CharField('название', max_length=255)
    description = models.TextField('описание', blank=True)
    creator = models.ForeignKey(User, verbose_name='кто создал задачу', related_name='tasks', on_delete=models.SET_NULL, null=True)
    deadline = models.DateTimeField('дедлайн', null=True)
    dc = models.DateTimeField('создан', auto_now_add=True)
    
